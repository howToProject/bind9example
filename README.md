# Bind9 DNS server

This is an example of a very simple DNS server. Up in no time. It may help for testing and experimenting in lab-networks.

## Notable files:

The file config/word contains many comments.

The file config/ms1 is a simple, working example.

Note that the zones must be set in config/named.conf

## Image version

Latest image is `9.18-22.04_beta` at the time of testing.

## Running the server
### Docker compose

Launch the server with

    docker-compose up

### Long command

    docker run --rm -d --name bind9 -e TZ=UTC -v $pwd/config:/etc/bind -e BIND9_USER=root -p 53:53 -p 53:53/udp ubuntu/bind9:9.18-22.04_beta  


## Testing

    nslookup mailserver1 localhost


## Notes

Exposing another port -p 30053:53 worked for the server. But querying did not work:

    nslookup -port=30053 mailserver1 localhost
    -> setting the port did not work!
    -> Use Port 53 instead, then the container gets queried

The server must be run with BIND9_USER=root.

Look at the paths of the volume mapping if you need to change something on a real server.